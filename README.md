# Implementation of an cloud-edge-architecture with Ansible and KubeEdge

## Project structure

####  Ansible Mario Lowa/: 
Ansible scripts

#### kubernetes_deployment_files/: 
Kubernetes configuration files for deploying applictions on the edge nodes

#### application.yaml
kubernetes configuration file for ArgoCD
####  Ansible Mario Lowa/roles/: 
In this folder are the different Ansible roles allowing to deploy the different components of the architecture

####  Ansible Mario lowa/roles/role_name/vars/: 

Here are the variables use for a particular role.

####   Ansible Mario lowa/hosts:
Here is the list of nodes that make up the cluster and the variables that Ansible uses to connect to the different nodes 



## Commands
#### cloud:
[ansible-playbook -i hosts cloud_init.yml] : command to fully configure the master node. Install docker, KubeEdge cloudcore and various Kubernetes components.

[ansible-playbook -i hosts --tags "kubernetes-master" cloud_init.yml]: to install only the Kubernetes components on the master node.

[ansible-playbook -i hosts --tags "cloudcore" cloud_init.yml]: to install only cloudcore on the master node.

[ansible-playbook -i hosts --tags "kubernetes-master, cloudcore" cloud_init.yml]: to install  cloudcore and kubernetes components on the master node.

[ansible-playbook -i hosts cloud_reset.yml]: command to reset the cloudcore and unsinstall all kubernetes components on the master node.

[ansible-playbook -i hosts --tags "cloudcore" cloud_reset.yml]: command to reset only the cloudcore

[ansible-playbook -i hosts --tags "kubernetes-master" cloud_reset.yml]: reset only the Kubernetes master components


####  edge:

[ansible-playbook -i hosts edgecore_init.yml] : command to fully configure an edge node. Install docker and install the edgecore

[ansible-playbook -i hosts --tags "edgecore" edgecore_init.yml]: command to only install the edgecore

[ansible-playbook -i hosts stop_edgecore.yml]: command to reset the edgecore

####  argocd:
[ansible-playbook -i hosts argocd_config.yaml] : command to fully install argocd

[ansible-playbook -i hosts aansible-playbook -i hosts --tags "kubernetes-master" cloud_init.ymlrgocd_reset.yaml] : command to reset argocd

##  Important steps for the initialization of the master node:
1 - install the Kubernetes components on the master node with the command: [ansible-playbook -i hosts --tags "kubernetes-master" cloud_init.yml]

2 - install ArgoCD with the command: [ansible-playbook -i hosts argocd_config.yaml]

3 - install the cloudcore: [ansible-playbook -i hosts --tags "cloudcore" cloud_init.yml] 
